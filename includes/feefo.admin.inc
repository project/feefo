<?php

/**
 * @file
 * Administrative forms for the PayPal WPP module.
 */

/*
 * Implements admin settings for feefo.
 */
function feefo_admin($form_state) {
  $form = array();

  $form['feefo_merchantID'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Merchant Identifier'),
    '#default_value' => variable_get('feefo_merchantID', ''),
  );
  $form['feefo_username'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Feefo login username'),
    '#default_value' => variable_get('feefo_username', ''),
  );
  $form['feefo_password'] = array(
    '#type' => 'password',
    '#required' => TRUE,
    '#title' => t('Feefo login password'),
    '#default_value' => variable_get('feefo_password', ''),
  );
  $form['feefo_api'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Feefo API key'),
    '#default_value' => variable_get('feefo_api', ''),
  );

  return system_settings_form($form);
}
